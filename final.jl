### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ e31258d2-d163-11eb-0432-bd716e8c0f68
begin
	using Pkg
	using FastAI
	using FastAI.Datasets
	using FastAI.Datasets: datasetpath, loadtaskdata
	using FastAI: Flux, Flux.Data.MNIST
	using Flux: onehotbatch, onecold, crossentropy, throttle
	using Base.Iterators: repeated, partition
	using Printf, BSON
	using ImageView
	using Plots
	using DataFrames
	#using MLDatasets
	using CUDA
	using Statistics, Random
	using Flux: @epochs
	using Images
	using ProgressMeter: @showprogress
end

# ╔═╡ 46a6c9cd-5dc7-4ab8-a782-a0b69a013db6
function resize_and_grayify(directory, im_name, width::Int64, height::Int64)
	j =directory * "\\" * im_name 
    resized_gray_img = Gray.(load(j)) |> (x -> imresize(x, width, height))
    try
        save("preprocessed_" *j , resized_gray_img)
    catch e
        if isa(e, SystemError)
            mkdir("preprocessed_" * directory)
            save("preprocessed_" *j , resized_gray_img)
        end
    end
end

# ╔═╡ 307e1e6a-c951-49e0-ae19-9cbdd45b9d23
function process_image(path)
    img = load(path)
    img = Gray.(img)
    img = imresize(img,(80,80))
    img = vec(img)
    img = convert(Array{Float64,1},img)
    return img
end

# ╔═╡ 4a71223d-3f03-48f1-a11e-09b40f6a1f66
function process_images(directory, width::Int64, height::Int64)
    files_list = readdir(directory)
    map(x -> resize_and_grayify(directory, x, width, height),                               files_list)
end

# ╔═╡ b8276d31-5604-492d-a8fa-4273fdd5945e
md"### Getting class"

# ╔═╡ 47c67ad8-fa27-4c6c-8ea6-04efc3ce7532
trin_dir ="C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\NORMAL\\"

# ╔═╡ 3786dfc6-e4f8-4c51-b951-00ab5842c1bd
trin_dir2 ="C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\"

# ╔═╡ 256be905-04e6-4366-9cd9-5856a860a88f
files_list = readdir(trin_dir2)

# ╔═╡ a241e039-3c80-4acf-98f7-89496a50b3be
n_resolution = 500

# ╔═╡ d9dbe882-5053-461a-a58f-ee72ac9b764f
d= pwd()

# ╔═╡ 40fab622-bf50-4861-867e-c142f26f3f4b
begin
	process_images("$d\\chest_xray\\train\\NORMAL", n_resolution, n_resolution)
    process_images("$d\\chest_xray\\train\\PNEUMONIA", n_resolution, n_resolution)
	process_images("$d\\chest_xray\\val\\NORMAL", n_resolution, n_resolution)
    process_images("$d\\chest_xray\\val\\PNEUMONIA", n_resolution, n_resolution);
end

# ╔═╡ 264e644f-70d8-479d-9976-115bfbd1c8af
md"## Get image name"

# ╔═╡ 91c7841a-b82c-4ae8-b0a9-a07ff913a814
begin
    t_norm_dir = readdir("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\NORMAL\\")

    t_pneu_dir = readdir("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\PNEUMONIA\\")
	
	v_norm_dir = readdir("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val\\NORMAL\\")
    v_pneu_dir = readdir("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val\\PNEUMONIA\\");
end;

# ╔═╡ a35ff047-39c8-4f29-90d7-cd504ab2d987
function resizeImage(img)
	## note decimate() takes two parameters 1. the image and 2. a int value 
	## the larger the int value the smaller the image will appear (2, 4, 5, 6, 8, 10)  ty this sizes
	#img_small = imresize(img, (100,100))#ratio=1/8)
	img = Gray.(img)
	img_small = img
	return img_small
	## this decrease the quality
end

# ╔═╡ 4700b77e-abec-48c8-8370-7843265087e3
root_val = "C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val\\NORMAL\\"

# ╔═╡ 23383f3e-a267-4faf-aa57-42f09c3cc42d
length(v_norm_dir)

# ╔═╡ 67338d32-00ef-4062-8ba7-5d20da8a8f0d
length(root_val)

# ╔═╡ d53af459-6f3a-46d3-9d8f-22f08680032b
begin
    # we load the pre-proccessed images
    train_norm = load.("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\NORMAL\\" .* t_norm_dir)
    train_pneu = load.("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train\\PNEUMONIA\\" .* t_pneu_dir)
	valid_norm = load.("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val\\NORMAL\\" .* v_norm_dir)
	valid_pneu = load.("C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val\\PNEUMONIA\\" .* v_pneu_dir);
end;

# ╔═╡ 6eee1a9e-fd1d-4819-afb5-0197ef390f97
typeof(train_norm)

# ╔═╡ 4dbc2489-4ced-44e8-8726-27f7fc7e2953
train_norm[1]

# ╔═╡ d78e8652-2dbb-43a6-9d20-807ab8d0691a
# we concatenate all bees and wasp images
train_data = vcat(train_norm, train_pneu);

# ╔═╡ 1d086026-2732-448d-82c7-7a76071fde1e
#x_train = train_data

# ╔═╡ f9a8911f-15d2-4247-8fed-b7637fe349fc
valid_data = vcat(valid_norm, valid_pneu);

# ╔═╡ 7c9304b0-afa9-4bca-9e3b-d63a26d9f1b1
#data = vcat(bees, wasps);

# ╔═╡ 94e3c850-62a8-4a05-a9e2-e11165030b89
#begin
#    labels = vcat([0 for _ in 1:length(train_norm)], [1 for _ in #1:length(train_pneu)])
#    (x_train, y_train), (x_test, y_test) = splitobs(shuffleobs((train_data, labels)), #at = 0.7)
#end;

# ╔═╡ ee9d61ee-3b0f-4435-8fb4-626257ddb513
md"## Labelling imagees with 0s and 1s "

# ╔═╡ c620da5b-2cd6-41b5-a052-626391cb87ad
# 0 = NORMAL
# 1 = PNEUMONIA

# ╔═╡ ba47b0f4-b415-4ab9-b889-8dd881b4e6fb
#begin
#	lables = vcat([0 for _ in 1:length(train_norm)], [1 for _ in #1:length(train_pneu)])
#	(x_train, y_train), (x_test, y_test) = splitobs(shuffleobs((train_data, lables)), #at = 0.8)
#end

# ╔═╡ e498013a-92d9-4f4e-b564-374dd63c8d09
begin
	lables = vcat([0 for _ in 1:length(train_norm)], [1 for _ in 1:length(train_pneu)])
	x_train, y_train = train_data, lables
end

# ╔═╡ 761ed393-8fe2-4d96-aa00-2a4165d79103
length(train_norm)

# ╔═╡ 945e45c5-de21-4ed5-aed9-1ed536048f98
length(train_pneu)

# ╔═╡ 949c5353-9e08-4332-96c1-2d396125bc50
md"## Testing and checking my data"

# ╔═╡ e93c840e-7b2c-4d89-84cb-61b75c12cf43
typeof(x_train)

# ╔═╡ db3c4541-166f-47e3-8eec-41262dbcdc22
#y_train = lables

# ╔═╡ 8aeaf44d-7a07-42d3-8333-1537d506189b
Y_batch = onehotbatch(lables[1:5216], 0:1)

# ╔═╡ c210cc6c-b84c-4a40-8954-b21ee739a6a6
typeof(Y_batch)

# ╔═╡ 00c27eab-d648-47b2-a602-9920e4239316
size(Y_batch)

# ╔═╡ 83d0d5df-8add-4e27-9dd4-baa77406c8d6
train_data

# ╔═╡ 5dcab00a-8380-4ddb-a1c0-a4f9c871af5d


# ╔═╡ ecdbe6dc-4055-4382-8624-ff012a79af14
size(train_data)

# ╔═╡ 6b4dfaed-ff55-4719-a52d-82daeb1bb47b
md"# Getting data ready for training"

# ╔═╡ e2329426-91ce-4a64-ab54-532f4fbb9117


# ╔═╡ 546c0680-a260-4090-9cd7-cd1365a05405
# this function creates minibatches of the data. Each minibatch is a tuple of (data, labels).
function make_minibatch(X, Y, idxs)
    X_batch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        X_batch[:, :, :, i] = Float32.(X[idxs[i]])
    end
    Y_batch = onehotbatch(Y[idxs], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 9db6fcfe-9ede-4e72-b946-da67e1897041
md"## Manual create a minibatch that works"

# ╔═╡ 5e661220-f25b-41d1-ba2b-571d36b3a82c
begin
    # here we define the train and test sets.
    batchsize = 128
    mb_idxs = partition(1:length(x_train), batchsize)
    train_set = [make_minibatch(x_train, y_train, i) for i in mb_idxs]
    test_set = make_minibatch(x_test, y_test, 1:length(x_test));
end;

# ╔═╡ bc7fec2d-95e3-4fa4-a9ce-dd1cb0b5ef50
train_total = 5216

# ╔═╡ 61736fbb-0ab0-4e57-b178-28dd0795c31b
function make_minibatch2(X, Y)
	
    X_batch = Array{Float32}(undef, size(resizeImage(X[1,1]))..., 1, train_total)
    for i in 1:train_total
        X_batch[:, :, :, i] = X[i,1]
    end
			# make lables zeros for normal and 1s for pneumia
	Y_batch = onehotbatch(lables[1:train_total], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 61f48860-1cb7-4189-9016-9bb54258c058
#train_set = make_minibatch2(x_train, y_train)

# ╔═╡ 2f87a931-5522-462a-87c6-a83eb138c060
mb_idxs2 = partition(1:length(x_train), 128)

# ╔═╡ 336891e1-e44e-404f-9cb7-a184f6f48770
length(x_test)

# ╔═╡ 30f2c26a-66e6-4fc5-b3fb-ccc23f9b20ba
sizeof(x_train)

# ╔═╡ 2e341ac5-5fb9-41e2-851b-ea631d11d86c
length(x_train)

# ╔═╡ 51004fb7-647b-48bb-9137-59271d6131b3
sizeof(y_train)

# ╔═╡ cad8f73b-d7f6-4d31-be9c-fa12fb246356
length(y_train)

# ╔═╡ 35b8461f-e104-4ad6-bd24-d52a0d8768f5


# ╔═╡ 893fe10a-c307-4a66-b521-08b6860522ca
md"# The Model"

# ╔═╡ 3d161114-f246-4078-9e1a-c125ebd6065a
md"## LeNet5 Function model"

# ╔═╡ 8c79ad66-0392-4516-8450-ae84365c4b5f


# ╔═╡ ed23a997-8ea5-43bc-8a5b-a6f16f67945f
# here we define our convolutional neural network model.
	# LeNet5 = 2 Convolutions and 3 Fully connected Layers (2+3 = 5)
	# n x n image with f x f filter = output size=(n-f+1) x (n-f+1)
model = Chain(
        Conv((3, 3), 1=>32, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3, 3), 32=>32, pad=(1,1), relu),
       # MaxPool((2,2)),
        #Conv((3, 3), 64=>128, pad=(1,1), relu),
        MaxPool((2,2)),
        #flatten,
        Dense(500, 120, relu), 
        Dense(120, 84, relu), 
        Dense(84, 2),
        softmax)

# ╔═╡ d008b20a-5717-48a9-b150-186fc640f7f0
md"# Creating the loss, accuracy functions"

# ╔═╡ fbd68998-4374-4bbe-85c5-ce119c886644
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    ps = Flux.params(model)
    opt = ADAM()
    L(x, y) = Flux.crossentropy(model(x), y)
    L((x,y)) = Flux.crossentropy(model(x), y)
    accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
    
    function update_loss!()
        push!(train_loss, mean(L.(train_set)))
        push!(test_loss, mean(L(test_set)))
        push!(acc, accuracy(test_set..., model))
        @printf("train loss = %.2f, test loss = %.2f, accuracy = %.2f\n", train_loss[end], test_loss[end], acc[end])
    end
end

# ╔═╡ a9596703-ccac-4da8-a3f2-f4264b25e799
# here we train our model for n_epochs times.
@epochs 10 Flux.train!(L, ps, train_set, opt;
               cb = Flux.throttle(update_loss!, 8))

# ╔═╡ 955d8e3b-487e-433b-b332-122f5fa98608
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
end

# ╔═╡ 38c1941b-3b0b-4e20-887f-5e361befaa5c
md"## we can change the weights here for optimization"

# ╔═╡ a6121d13-7499-42d8-b4b5-3f550a16ac86
opt_2 = ADAM(0.0005)

# ╔═╡ 19cefe53-7eb3-4faf-9135-20ef38cce884
@epochs 5 Flux.train!(L, ps, train_set, opt_2;
               cb = Flux.throttle(update_loss!, 8))

# ╔═╡ 0c36b3df-3a82-42a5-b495-63d4950a8781
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9, legend = :right)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
    vline!([82], lw=2, label=false)
end

# ╔═╡ a198a5fd-4230-47b3-bd6f-067d625b166e
begin
	epochs = 10
	
	for epoch = 1:epochs
  		for d in eachrow(train_set)
    		gs = gradient(params(model)) do
      		l = loss(d...)
	   	end
    	update!(opt, params(model), gs)
	end
  	@show accuracy(valX, valY)
end
end

# ╔═╡ cf5d4cc9-86e0-469b-bbcd-100bba07c1d7
function fit!(model::Flux.Chain, features, labels; silent=true, max_epochs=100_000, conv_atol=0.005, conv_period=5)
    function loss(x, y) # AFAIK Flux loss must *always* be a closure around the model (or the model be global)
        ŷ = model(x)
        -sum(log.(ifelse.(y, ŷ, 1-ŷ))) #Binary Cross-Entropy loss
    end
    
    old_loss_o = Inf
    conv_timer = conv_period   
    function stop_cb(ii,loss_val)
        loss_o = loss_val/length(labels) #real loss is the sum, but we want to use the mean
        if loss_o < old_loss_o - conv_atol
            conv_timer = conv_period
        else
            conv_timer-=1
            if conv_timer < 1
                return true
            end
        end
        old_loss_o = loss_o
        return false
    end
    
    log_cb(ii, loss_val) = println("at $ii loss: ", loss_val/length(labels))
    
    opt = ADAM(params(model))
    dataset = Base.Iterators.repeated((features, labels), max_epochs)
    Flux.train!(loss, dataset, opt,
        log_cb = Flux.throttle(silent ? (i,l) -> () : log_cb, 10), # every 10 seconds
        stopping_criteria = iter_throttle(stop_cb, 100), #Every 100 rounds
    )
    
    model
end

# ╔═╡ a3d03d84-3beb-4617-b754-46c4d48dfb85
function evaluate(modeltype)    
    @time model = fit!(modeltype, x_train, y_train)
	    
    println("$modeltype Train accuracy: ", percent(accuracy(model, train_features, train_labels)))
    println("$modeltype Test accuracy: ", percent(accuracy(model, test_features, test_labels)))
	    
    #this is calculating the predict twice (since we did it to report accuaracy already), but predict is cheap
    plotflag(test_features, predict(model, test_features); title=string(modeltype))
end

# ╔═╡ a873d285-cbc0-470d-9a23-7fb4467e19d3
evaluate(model)

# ╔═╡ Cell order:
# ╠═e31258d2-d163-11eb-0432-bd716e8c0f68
# ╠═46a6c9cd-5dc7-4ab8-a782-a0b69a013db6
# ╠═307e1e6a-c951-49e0-ae19-9cbdd45b9d23
# ╠═4a71223d-3f03-48f1-a11e-09b40f6a1f66
# ╠═b8276d31-5604-492d-a8fa-4273fdd5945e
# ╠═256be905-04e6-4366-9cd9-5856a860a88f
# ╠═47c67ad8-fa27-4c6c-8ea6-04efc3ce7532
# ╠═3786dfc6-e4f8-4c51-b951-00ab5842c1bd
# ╠═a241e039-3c80-4acf-98f7-89496a50b3be
# ╠═d9dbe882-5053-461a-a58f-ee72ac9b764f
# ╠═40fab622-bf50-4861-867e-c142f26f3f4b
# ╠═264e644f-70d8-479d-9976-115bfbd1c8af
# ╠═91c7841a-b82c-4ae8-b0a9-a07ff913a814
# ╠═a35ff047-39c8-4f29-90d7-cd504ab2d987
# ╠═4700b77e-abec-48c8-8370-7843265087e3
# ╠═23383f3e-a267-4faf-aa57-42f09c3cc42d
# ╠═67338d32-00ef-4062-8ba7-5d20da8a8f0d
# ╠═d53af459-6f3a-46d3-9d8f-22f08680032b
# ╠═6eee1a9e-fd1d-4819-afb5-0197ef390f97
# ╠═4dbc2489-4ced-44e8-8726-27f7fc7e2953
# ╠═d78e8652-2dbb-43a6-9d20-807ab8d0691a
# ╠═1d086026-2732-448d-82c7-7a76071fde1e
# ╠═f9a8911f-15d2-4247-8fed-b7637fe349fc
# ╠═7c9304b0-afa9-4bca-9e3b-d63a26d9f1b1
# ╠═94e3c850-62a8-4a05-a9e2-e11165030b89
# ╠═ee9d61ee-3b0f-4435-8fb4-626257ddb513
# ╠═c620da5b-2cd6-41b5-a052-626391cb87ad
# ╠═ba47b0f4-b415-4ab9-b889-8dd881b4e6fb
# ╠═e498013a-92d9-4f4e-b564-374dd63c8d09
# ╠═761ed393-8fe2-4d96-aa00-2a4165d79103
# ╠═945e45c5-de21-4ed5-aed9-1ed536048f98
# ╠═949c5353-9e08-4332-96c1-2d396125bc50
# ╠═e93c840e-7b2c-4d89-84cb-61b75c12cf43
# ╠═db3c4541-166f-47e3-8eec-41262dbcdc22
# ╠═8aeaf44d-7a07-42d3-8333-1537d506189b
# ╠═c210cc6c-b84c-4a40-8954-b21ee739a6a6
# ╠═00c27eab-d648-47b2-a602-9920e4239316
# ╠═83d0d5df-8add-4e27-9dd4-baa77406c8d6
# ╠═5dcab00a-8380-4ddb-a1c0-a4f9c871af5d
# ╠═ecdbe6dc-4055-4382-8624-ff012a79af14
# ╠═6b4dfaed-ff55-4719-a52d-82daeb1bb47b
# ╠═e2329426-91ce-4a64-ab54-532f4fbb9117
# ╠═546c0680-a260-4090-9cd7-cd1365a05405
# ╠═9db6fcfe-9ede-4e72-b946-da67e1897041
# ╠═61736fbb-0ab0-4e57-b178-28dd0795c31b
# ╠═5e661220-f25b-41d1-ba2b-571d36b3a82c
# ╠═bc7fec2d-95e3-4fa4-a9ce-dd1cb0b5ef50
# ╠═61f48860-1cb7-4189-9016-9bb54258c058
# ╠═2f87a931-5522-462a-87c6-a83eb138c060
# ╠═336891e1-e44e-404f-9cb7-a184f6f48770
# ╠═30f2c26a-66e6-4fc5-b3fb-ccc23f9b20ba
# ╠═2e341ac5-5fb9-41e2-851b-ea631d11d86c
# ╠═51004fb7-647b-48bb-9137-59271d6131b3
# ╠═cad8f73b-d7f6-4d31-be9c-fa12fb246356
# ╠═35b8461f-e104-4ad6-bd24-d52a0d8768f5
# ╠═893fe10a-c307-4a66-b521-08b6860522ca
# ╠═3d161114-f246-4078-9e1a-c125ebd6065a
# ╠═8c79ad66-0392-4516-8450-ae84365c4b5f
# ╠═ed23a997-8ea5-43bc-8a5b-a6f16f67945f
# ╠═a3d03d84-3beb-4617-b754-46c4d48dfb85
# ╠═a873d285-cbc0-470d-9a23-7fb4467e19d3
# ╠═d008b20a-5717-48a9-b150-186fc640f7f0
# ╠═fbd68998-4374-4bbe-85c5-ce119c886644
# ╠═a9596703-ccac-4da8-a3f2-f4264b25e799
# ╠═955d8e3b-487e-433b-b332-122f5fa98608
# ╠═38c1941b-3b0b-4e20-887f-5e361befaa5c
# ╠═a6121d13-7499-42d8-b4b5-3f550a16ac86
# ╠═19cefe53-7eb3-4faf-9135-20ef38cce884
# ╠═0c36b3df-3a82-42a5-b495-63d4950a8781
# ╠═a198a5fd-4230-47b3-bd6f-067d625b166e
# ╠═cf5d4cc9-86e0-469b-bbcd-100bba07c1d7
