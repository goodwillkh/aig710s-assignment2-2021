### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 80ac52b0-1118-47a1-b644-549d9ea6016d
using Pkg

# ╔═╡ d687707a-671d-44bb-a0ad-7f7b76018ae9
Pkg.add("MLJFlux")

# ╔═╡ 7c341986-6527-4ded-8dae-53e79af1f19a
Pkg.add("Plots")

# ╔═╡ b8213dcc-2d8b-491e-bb83-cf0a5fa38667
Pkg.add("Images")

# ╔═╡ d8af6824-043a-4ab9-b4d3-34440a43717a
Pkg.add("ImageFeatures")

# ╔═╡ c6609c1c-b221-4e05-834f-bb2923ea5bd4
Pkg.add("ImageMagick")

# ╔═╡ b85937f6-c5f5-487f-9244-bea448c0d054
Pkg.add("PyCall")

# ╔═╡ 3516264b-91e5-4751-9aa6-a38921d56362
Pkg.add("TestImages")

# ╔═╡ 5d8c9ed4-9223-43ab-a1ef-c50e7a0cb22c
Pkg.add("MLDataUtils")

# ╔═╡ 3d863e14-851c-4670-a80c-c2898a870d44
Pkg.add("MLDatasets")

# ╔═╡ 24cc824b-5244-4e22-92a9-284263c8ecbd
Pkg.add("CSV")

# ╔═╡ 86ad42aa-ef9c-4508-8233-3464547358e0
Pkg.add("Zygote")

# ╔═╡ 9ac1f6c9-fcbb-44fd-93c6-cd0dd0a85807
Pkg.add("CUDA")

# ╔═╡ 704dd474-a7a6-4dd8-a425-8d8008a8fb5e
Pkg.add("ProgressMeter")

# ╔═╡ 440de24a-c966-467d-8f3c-111724ca7d9e
Pkg.add("ImageView")

# ╔═╡ b930ead7-02dd-42b0-ad45-29657f7e598b
Pkg.add("FileIO")

# ╔═╡ 92943ab0-3b1e-4039-87d9-39115919e48e
using PlutoUI

# ╔═╡ f9650d8b-d5fc-4a7e-9e94-6d8239f9b4df
using Flux

# ╔═╡ 2296e417-cbbc-4edd-92f6-ea34db573611
using CSV

# ╔═╡ 6602335e-2922-4cdd-a6bc-ea3c7ad79e42
using Zygote

# ╔═╡ 3a15d0f7-6932-44ae-a37b-1d5cd8d4e9e5
using MLDatasets

# ╔═╡ b73fe2f5-a95e-4e2b-94e3-d53c801f2b69
using MLDataUtils

# ╔═╡ 574e4d7e-26b7-433a-9786-dd51d68a09f0
using Images,ImageFeatures,ImageMagick

# ╔═╡ 777b7878-463d-491f-aa97-2aeb3ea4681a
using MLJFlux

# ╔═╡ 1906c863-cc25-48a2-b821-ee5e60789da5
using Plots

# ╔═╡ 60421479-e0b2-4add-86e9-6445a4af13ae
using CUDA

# ╔═╡ 3ac3b3c4-e8a2-4a83-acbe-9fe711fe3fb4
using Flux:Data.DataLoader

# ╔═╡ 3e9dbeed-0229-458c-8a97-532648d662d6
using Flux: @epochs

# ╔═╡ edc262eb-e82c-45ec-aac3-e8542b2d12d8
using Flux.Optimise: Optimiser, WeightDecay

# ╔═╡ 47b61091-a23d-4d29-aa6a-f333d360d9be
using Flux.Losses: logitcrossentropy

# ╔═╡ 6dc07238-4c73-4a9b-88c4-a3094388ee82
using ProgressMeter: @showprogress

# ╔═╡ 74ff4a0d-4c36-44f3-a238-b7403ad75333
using Flux: onehotbatch, onecold, crossentropy, throttle

# ╔═╡ 6c5f955b-b90d-4872-bcd7-91316bb0b9f9
using Statistics, Random

# ╔═╡ d4885d98-dece-4e56-99ab-2858f9c050eb
using DataFrames

# ╔═╡ 0c85c2f3-e87b-42f3-bf0b-3d0f89aa9092
using ImageView

# ╔═╡ 8503cc46-af66-4272-a3aa-b71615d85b1d
using Printf

# ╔═╡ 6832e6ac-0c96-412f-947f-5e098083a0ac
using FileIO

# ╔═╡ a6793577-00c9-425a-8006-fa1bb80062d1
using FastAI

# ╔═╡ 351a2520-c702-11eb-338c-9b993f8f510e
md"## Loading the various pacakes"

# ╔═╡ d214d01f-0c07-4097-a703-d2736d8fcab6


# ╔═╡ f4ca0f94-e6cf-453e-b0a6-9e4c0798f6b6
import BSON

# ╔═╡ 351878f6-c420-4e3d-8b4b-4d05e14484b3
Pkg.instantiate()

# ╔═╡ 83da4182-f902-4704-8b68-0de73811d5b1
md"## Data (image) Preprocessing"

# ╔═╡ 05bf8b65-ff94-4d5c-9021-b75f50418d2c
ENV["DATADEPS_ALWAYS_ACCEPT"] = true

# ╔═╡ d9e0befd-c8c7-4c83-8fcb-3227c7a7a1b3
# to see what the path looks like
pwd()

# ╔═╡ 29658d7c-35d1-41ec-9e03-04e1bd9986d6
target_dir = "C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2"


# ╔═╡ acdf7377-0e23-4b2b-8298-e767d5cc0844
md"### Define Directories for train, test & Validation Set"

# ╔═╡ 927117df-8cf3-4741-8c59-583648062546
begin
	train_path ="C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\train"
	test_path ="C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\test"
	valid_path ="C:\\Users\\goodw\\OneDrive\\Desktop\\NUST_STUDIES\\Semester_5\\AIG\\Assignments\\Assignment2\\chest_xray\\val"
end

# ╔═╡ 8dc2d5bf-8de1-4de5-8bb5-a657ecfb9178
md"#### This will get the final path of the images and the image names follows"

# ╔═╡ 203bc150-c1e5-483a-941d-66e7bf7236a6
normal_train = "$train_path\\NORMAL\\"

# ╔═╡ 31654e81-2f5c-410a-abf0-3dd1ede281bb
normal_train_data = readdir(normal_train)

# ╔═╡ 477a3c4e-c253-45e8-8669-52b09bbcebd9
normal_test = "$test_path\\NORMAL\\"

# ╔═╡ 95028509-0a9d-4b71-80e1-ec1faf163fc4
normal_test_data = readdir(normal_test)

# ╔═╡ be48a4f7-8f86-4491-8955-29875b655805
normal_val = "$valid_path\\NORMAL\\"

# ╔═╡ 80e0e5b5-c7f5-400b-bbf0-764722f4857d
normal_val_data = readdir(normal_val)

# ╔═╡ 20778a44-9188-4319-95ce-8257c38d0e0f
pneumonia_train = "$train_path\\PNEUMONIA\\"

# ╔═╡ e89529b9-d8bf-4532-a3e6-bde93d7f51f7
pneumonia_train_data = readdir(pneumonia_train)

# ╔═╡ 428f8ec3-f03b-420d-9b43-79a5126981a6
pneumonia_test = "$test_path\\PNEUMONIA\\"

# ╔═╡ 5ed77470-0e60-42c3-aa72-e72e05ad7a6c
pneumonia_test_data = readdir(pneumonia_test)

# ╔═╡ 66df1682-2814-45ff-b050-1cb6b00f1b5c
pneumonia_val = "$valid_path\\PNEUMONIA\\"

# ╔═╡ 1311bbee-937a-4471-b7e3-592e8a6a4221
pneumonia_val_data = readdir(pneumonia_val)

# ╔═╡ d73a8509-eabf-42b8-9922-d9f929fe4cef
# setting the batch size for each iteration of training

# ╔═╡ 49982ee6-31eb-4e18-ace1-e6dafe360aaf
n_pos_train = length(readdir(normal_train))

# ╔═╡ 33e6880e-1707-4197-a3af-1f55430b109e
n_pos_test = length(readdir(normal_test))

# ╔═╡ 2f5f5e87-bf36-4a97-92bc-dbe87a92d9a1
n_pos_val = length(readdir(normal_val))

# ╔═╡ 1c2d9bd7-6c45-4033-9438-d6f1a1577345
p_neg_train =length(readdir(pneumonia_train))

# ╔═╡ dd001ba3-218f-421a-b0b3-9c38e5fe2c10
p_neg_test =length(readdir(pneumonia_test))

# ╔═╡ 05cae053-5774-40bc-b1be-55bf2be3a4bf
p_neg_val =length(readdir(pneumonia_val))

# ╔═╡ b744c017-a08f-4fda-a1db-7dc8451f653e
# setting the batch size for each iteration of training

# ╔═╡ 2d1fed46-5375-4d9c-a756-353294bb8c63
train_total = n_pos_train + p_neg_train

# ╔═╡ 9a36a705-9aec-45b4-aa46-dc57bd6c80fc
test_total = n_pos_test + p_neg_test

# ╔═╡ 1a89f0a7-e930-4c61-ab36-2aaf9d4f3b96
val_total = n_pos_val + p_neg_val

# ╔═╡ bb06c2c6-bc46-444e-9148-00345a43f3a7
#The dimension of the images we are going to define is 300x300 
img_height = 500

# ╔═╡ dc791d87-5a59-42a8-bd55-df7f811e857e
img_width = 500

# ╔═╡ 296a3b0c-eb5c-422c-98eb-5e537928b1aa
#we can make them larger but due to system processing and runtime we need smaller img

# ╔═╡ 506bd217-7c47-4922-b38c-496d98d317ef
md"### A function to get all data set for train or test if called"

# ╔═╡ 8c9ed2c0-7fab-47d8-9b82-890e177bf68b
# mode 1 = Train, mode 2 = test, mode 0 = valid
# path is the path to the mode
# filename is what we identified in the mode dir
function imageIter(mode)
	if mode == 0
		dataset_normal = normal_val.*normal_val_data
		dataset_pneumonia = pneumonia_val.*pneumonia_val_data
	elseif mode == 1
		dataset_normal = normal_test.*normal_test_data
		dataset_pneumonia = pneumonia_test.*pneumonia_test_data
	elseif mode == 2
		dataset_normal = normal_train.*normal_train_data
		dataset_pneumonia = pneumonia_train.*pneumonia_train_data
	end
	#combines both negative dataset as well as positive data set
	dataset = [dataset_normal; dataset_pneumonia]
	return dataset
end

# ╔═╡ f6d05acc-8e0b-49d5-a9e0-a60928205b16
img_paths = DataFrame(img = imageIter(2))

# ╔═╡ e27773f3-ec05-4582-8d6d-0a1c62451377
img2_paths = DataFrame(img = imageIter(1))

# ╔═╡ 8f83876a-006f-4873-9706-58bc864c24db
img3_paths = DataFrame(img = imageIter(0))

# ╔═╡ 5e3fd4ab-120e-475a-8843-1e8af0f31ba0


# ╔═╡ d2f8d36f-16fb-4149-b69b-c943b2251510
md"## defining the two Classes"

# ╔═╡ 6cc8e24a-dd5f-4a74-b32e-58fc47736486
classes = ["Normal", "Pneumonia"]

# ╔═╡ 9bb8f392-e9d0-4702-88e7-ab4fc83d8d79
all_Classes = CSV.read("All Lables.csv",DataFrame)

# ╔═╡ ffad8e29-319f-4dbe-b621-fd515f7c37b6
norm_Class = CSV.read("Normal Only.csv",DataFrame)

# ╔═╡ a00c834d-93d2-4cbf-8c9d-17d8440703e8
x_pos = size(norm_Class[!,1])

# ╔═╡ d5c789bc-fc60-4467-b4cc-64fd62ca7a08
pneum_Class = CSV.read("Pneumonia.csv",DataFrame)

# ╔═╡ 8b840944-6768-479d-b99e-f134bf4f2ccd
x_neg = size(pneum_Class[!,1])

# ╔═╡ 3fab9294-53aa-4775-8f6a-f2a7596b3227
md"### Generating the Data sets"

# ╔═╡ 27f6123d-47ca-4cd4-8137-2b916d4e5a51
x_train, y_train = img_paths, all_Classes

# ╔═╡ 327b9ea2-5740-4448-b6af-b983f6ff43a1
md"### Combining the images with the lables "

# ╔═╡ c9d6377e-cb37-45d6-a00e-6ff2503934b9
dataSet = hcat(x_train, y_train)

# ╔═╡ ceaf15d4-6f40-4ce3-be2e-e0d3dd175471
x_dataset = [ [row.img, row.Classes] for row in eachrow(dataSet)]

# ╔═╡ b595d1b0-b9e5-463d-97af-e087a727404a
typeof(x_dataset)

# ╔═╡ de23c14a-484d-4c89-8bc8-9d33cf39c515


# ╔═╡ 73299f2c-1b8d-468a-aef2-5d58cbc264bb
md"### Example loading an image "

# ╔═╡ 35c91e9e-8e27-48ff-abcd-0a7d7d6105bf
tester= load("$train_path\\NORMAL\\IM-0115-0001.jpeg")

# ╔═╡ fe1b38d5-f74b-43d6-9a86-f6d9aae664c6
rows, cols = size(tester)

# ╔═╡ 1e8608c4-1a5f-4ed1-b488-66b25ca7c922
md"### All images will be converted to size 500 x 500"


# ╔═╡ 9796ecbc-e37e-42b9-823e-69e904f8a4c6
md"#### Resize function "

# ╔═╡ dbaff6ba-4e78-4efb-ae77-b44344b981bf
function resizeImage(img)
	## note decimate() takes two parameters 1. the image and 2. a int value 
	## the larger the int value the smaller the image will appear (2, 4, 5, 6, 8, 10)  ty this sizes
	img_small = imresize(img, (100,100))#ratio=1/8)
	return img_small
	## this decrease the quality
end

# ╔═╡ 7043cca1-228d-412f-95d1-0af9cff6d514
md"#### Converting and Processing "

# ╔═╡ 9f83adc3-d6aa-45c5-83c0-db51b39bd9d8
function process_images(path)
    img = load(path)
    img = Gray.(img)
	img = resizeImage(img) #(500x500)
    #img = imresize(img,(80,80))
    img = vec(img)
    img = convert(Array{Float64,1},img)
    return img
end

# ╔═╡ 6235d73a-3f6d-41c9-81ce-831a1b08754d
small_tester = resizeImage(tester)

# ╔═╡ 6387a898-8150-4582-81bb-6c682375420c
size(tester)

# ╔═╡ ec51f9ab-0983-476f-99ff-7d036fc23133
md"#### check processed image size"

# ╔═╡ db67d255-bd19-4827-9e65-216520e58fcd
size(small_tester)

# ╔═╡ ea0835e0-dce7-4159-a5b5-b1489199d6c0


# ╔═╡ feffb102-420e-4533-8333-26fcb60a112e
md"# YOU Coded here to add .image at the end  "

# ╔═╡ b09828a5-adbc-4181-ae7f-f052f34e6bf5
train_imgs = imageIter(0)

# ╔═╡ a046cd4b-f985-4fbe-9549-cc9e7b1ea887
train_imgs2 = Vector{Matrix{Gray{Normed{UInt8,8}}}}()

# ╔═╡ ec6459f4-9a2a-4205-8285-dade22d3abbc
train_imgs2b = Array{Array{Gray{Normed{UInt8, 8}}, 2}, 1}

# ╔═╡ b812ce2a-14ab-4238-b4ff-ba6c85834336
size(x_train[!,1])

# ╔═╡ ac5a548a-b0b2-42ba-8f67-709b99f72cf4
size(y_train[!,1])

# ╔═╡ 488112c3-c3ed-4ac0-b1d6-1d53be88131f
size(img3_paths)

# ╔═╡ f5c21e7d-ef41-4e5b-b512-3bf9373144a3
ot = y_train[4000,1]

# ╔═╡ 6a3acf57-45b2-4841-8325-83e955e6dc72
o = size(resizeImage(load(x_train[1,1])))

# ╔═╡ 3beb5d0f-85f4-4ff6-96a4-29f0b1a9f643
function process_images2(path)
    img = load(path)
    img = Gray.(img)
	img = resizeImage(img) #(500x500)
    #img = imresize(img,(80,80))
    img = vec(img)
    img = convert(Array{Float64,1},img)
    return img
end

# ╔═╡ a1d1d9e3-d86f-4498-94c4-c6769c09ad7d
# I had To test each Line to see if it is prducing what I need

# ╔═╡ 363bb90b-314b-4fbd-883f-c25c3d968700
lables = vcat([0 for _ in 1:n_pos_train], [1 for _ in 1:p_neg_train
])

# ╔═╡ 102ff106-33e3-41fe-9df3-209c958be15f
function make_minibatch2(X, Y, n, p)
	
    X_batch = Array{Float32}(undef, size(resizeImage(load(X[1,1])))..., 1, train_total)
    for i in 1:train_total
        X_batch[:, :, :, i] = process_images2(X[i,1])
    end
			# make lables zeros for normal and 1s for pneumia
	Y_batch = onehotbatch(lables[1:train_total], 0:1)
    return (X_batch, Y_batch)
end

# ╔═╡ 29c9c6f9-cf0c-470e-8791-18fb46de051b
sizeof(lables)

# ╔═╡ d91889c9-eaf1-4f8c-be1b-afa5b60c1a50
Y_batch = onehotbatch(lables[1:train_total], 0:1)

# ╔═╡ e287127f-3042-4b2b-88bf-0dfa27ed34db
#Y_batch = [fill(0, x_pos); fill(1, x_neg)]

# ╔═╡ fcefa967-876b-426a-83e6-ecfcaad5c6ef
typeof(Y_batch)

# ╔═╡ fb71ae63-0391-4605-b561-4862346a8eb4
tryDataSet = make_minibatch2(x_train, y_train, x_pos, x_neg)

# ╔═╡ 70dff569-31f4-4b20-bf9d-8b7b2fe8166a
size(tryDataSet[1][2])

# ╔═╡ 0b8ed630-978b-4dc1-abab-2ac439aa6f76
typeof(tryDataSet)

# ╔═╡ 785388d2-a417-444b-a1ce-02fa2438d585
m = Array{String,1}

# ╔═╡ bfcaf949-79ac-415a-8725-61486c5e8d68
md"## The Model"

# ╔═╡ d454d0b8-b700-477c-a700-5d854b85e94c
# here we define our convolutional neural network model.
	# LeNet5 = 2 Convolutions and 3 Fully connected Layers (2+3 = 5)
	# n x n image with f x f filter = output size=(n-f+1) x (n-f+1)
model = Chain(
        Conv((3, 3), 1=>32, pad=(1,1), relu),
        MaxPool((2,2)),
        Conv((3, 3), 32=>32, pad=(1,1), relu),
       # MaxPool((2,2)),
        #Conv((3, 3), 64=>128, pad=(1,1), relu),
        MaxPool((2,2)),
        #flatten,
        Dense(500, 120, relu), 
        Dense(120, 84, relu), 
        Dense(84, 2),
        softmax)

# ╔═╡ 311cd8e6-bce3-4b74-81b8-cba4535dde0f
function fit!(model::Flux.Chain, features, labels; silent=true, max_epochs=100_000, conv_atol=0.005, conv_period=5)
    function loss(x, y) # AFAIK Flux loss must *always* be a closure around the model (or the model be global)
        ŷ = model(x)
        -sum(log.(ifelse.(y, ŷ, 1-ŷ))) #Binary Cross-Entropy loss
    end
    
    old_loss_o = Inf
    conv_timer = conv_period   
    function stop_cb(ii,loss_val)
        loss_o = loss_val/length(labels) #real loss is the sum, but we want to use the mean
        if loss_o < old_loss_o - conv_atol
            conv_timer = conv_period
        else
            conv_timer-=1
            if conv_timer < 1
                return true
            end
        end
        old_loss_o = loss_o
        return false
    end
    
    log_cb(ii, loss_val) = println("at $ii loss: ", loss_val/length(labels))
    
    opt = ADAM(params(model))
    dataset = Base.Iterators.repeated((features, labels), max_epochs)
    Flux.train!(loss, dataset, opt,
        log_cb = Flux.throttle(silent ? (i,l) -> () : log_cb, 10), # every 10 seconds
        stopping_criteria = iter_throttle(stop_cb, 100), #Every 100 rounds
    )
    
    model
end

# ╔═╡ 8fe23f8a-8d4c-4a35-8603-9682e533ae37
md"# Creating the loss, accuracy functions"

# ╔═╡ 3ccf8ceb-b42a-4db0-81a8-f9f1157c39ee
begin
    train_loss = Float64[]
    test_loss = Float64[]
    acc = Float64[]
    ps = Flux.params(model)
    opt = ADAM()
    L(x, y) = Flux.crossentropy(model(x), y)
    L((x,y)) = Flux.crossentropy(model(x), y)
    accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
    
    function update_loss!()
        push!(train_loss, mean(L.(tryDataSet)))
        push!(test_loss, mean(L(test_set)))
        push!(acc, accuracy(test_set..., model))
        @printf("train loss = %.2f, test loss = %.2f, accuracy = %.2f\n", tryDataSet[end], test_loss[end], acc[end])
    end
end

# ╔═╡ 694824e8-d798-4455-a5bb-7593d4aac5e4
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
end

# ╔═╡ 3ce8e7b1-9183-4648-91d4-cab44588e14c


# ╔═╡ 9306fa27-1679-4896-b03f-7adb78af67ba
opt_2 = ADAM(0.0005)

# ╔═╡ c13e099f-5425-4ffc-b452-9c0403251a3f
@epochs 5 Flux.train!(L, ps, tryDataSet, opt_2;
               cb = Flux.throttle(update_loss!, 8))

# ╔═╡ d4c4aed1-b7ca-4ff3-853b-792324235f52
model(tryDataSet)

# ╔═╡ a70f0481-acdd-4427-b39f-8b1c8301083a
md"## LeNet5 Function"

# ╔═╡ ecdccbf2-a102-40ce-8ed2-a4b8ddf29332
# the flux chain model

# ╔═╡ 8f9598c2-13f0-41e1-8150-efa27e78454f


# ╔═╡ fe4eb90e-30af-404b-9fbf-1be150bba8f4
md"### Training the modle"

# ╔═╡ 038dbc83-4268-4a3e-a141-bec2ab4f459c


# ╔═╡ ceb65c0b-133d-4acc-9039-7b21aa1c6d95
## Adds class to image

# ╔═╡ fb93e546-fdbd-46db-b20f-a4a9474ec55e
## combines image with class

# ╔═╡ 4f9ce79b-e18b-428f-acf6-86e9d1c024bd
##assigns image to train and class to val

# ╔═╡ 3159e972-bb5d-4ecd-a8e9-25ccf27b87a8
md"## Evaluation Of The Model"

# ╔═╡ 6eafb819-a69c-4fd8-9f79-a7871cc4f237


# ╔═╡ 5a4b07bb-98e8-4690-a76f-e8233790ae23


# ╔═╡ 7f21d05e-25b8-4987-8c25-24e233ca8fb0
md"### Testing the modle"

# ╔═╡ a9ac8cfc-7a06-4d12-a00f-74bc2056e860


# ╔═╡ f4da16ac-9e94-455a-87af-a4c4a87b864a


# ╔═╡ 3a5dd253-75a8-4502-8d40-c0ff4f72e4af


# ╔═╡ ee4b1560-1f51-420c-87cb-66d94a1689e6
md"## Confusion Matrix" 

# ╔═╡ e15c142d-6bd8-4c06-ae6b-403f73645632
begin
    plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9, legend = :right)
    plot!(test_loss, label="Test loss", lw=2, alpha=0.9)
    plot!(acc, label="Accuracy", lw=2, alpha=0.9)
    vline!([82], lw=2, label=false)
end

# ╔═╡ f4e5432b-ff24-4690-b849-aca10c3ed23e


# ╔═╡ 3e191b6f-8d42-4c37-bec9-593412429469


# ╔═╡ 90aa2da3-c11c-412c-bb84-1fb7648f97ec
md"#### The Accuracy, Precision and Recall"

# ╔═╡ e4cee5d5-5389-4877-99bb-d481fe4ff372
## You have to use with terminal to view the results


# ╔═╡ 585ffa67-9615-4d8a-a9ff-1516fa2b66e0
begin
	epochs = 10
	
	for epoch = 1:epochs
  		for d in tryDataSet
    		gs = gradient(params(model)) do
      		l = loss(d...)
	   	end
    	update!(opt, params(model), gs)
	end
  	@show accuracy(valX, valY)
end
end

# ╔═╡ 9cc61f01-2469-41fa-b4bf-11c0b8bee278


# ╔═╡ 89ba50a8-6475-4142-9e30-a60a8ed3e7c8
md"## The Overall Solution"

# ╔═╡ a81728bd-184b-41f0-bad7-fe04b18f45bc


# ╔═╡ fd5b95e0-4eb7-45fa-94a6-582bea6834a1
# friom https://towardsdatascience.com/deep-learning-with-julia-flux-jl-story-7544c99728ca

# ╔═╡ a862955d-b00e-4c84-90a3-3795eed0fe38
md"## Deep Learning with Julia, Flux.jl story "

# ╔═╡ 56b335e5-4ab9-4bef-af59-bbb99f28e098
#Trying Other stuff

# ╔═╡ a06c8f73-23af-476d-8900-915ff937989b
x1_train, y1_train = MLDatasets.MNIST.traindata();

# ╔═╡ 377b7210-a98e-4bf2-aef6-5462e5d513ca
typeof(x1_train)

# ╔═╡ a20d29b0-6674-4efe-b500-2dd82979e2b8
x1_train;

# ╔═╡ 03b6fb0f-d726-4185-8cbf-1fd2ae44af10
typeof(y1_train)

# ╔═╡ 40698c47-e271-4729-9d07-aa79662fdd6c
y1_train

# ╔═╡ c5df8c92-1bff-4e7a-93c9-d3c0d5b9e221
x1_train2 = Flux.unsqueeze(x1_train, 3)

# ╔═╡ 02a93a00-ec59-4bc0-9489-88fdf97475d7
y1_train2 = onehotbatch(y1_train, 0:9)

# ╔═╡ 7cc9b806-0865-40b7-8873-8791f6610c04
function evaluate(modeltype)    
    @time model = fit!(modeltype, x1_train2, y1_train2)
	    
    println("$modeltype Train accuracy: ", percent(accuracy(model, train_features, train_labels)))
    println("$modeltype Test accuracy: ", percent(accuracy(model, test_features, test_labels)))
	    
    #this is calculating the predict twice (since we did it to report accuaracy already), but predict is cheap
    plotflag(test_features, predict(model, test_features); title=string(modeltype))
end

# ╔═╡ 64842ec8-2dbd-4064-afce-330690c064b2
evaluate(model)

# ╔═╡ Cell order:
# ╠═351a2520-c702-11eb-338c-9b993f8f510e
# ╠═80ac52b0-1118-47a1-b644-549d9ea6016d
# ╠═92943ab0-3b1e-4039-87d9-39115919e48e
# ╠═d214d01f-0c07-4097-a703-d2736d8fcab6
# ╠═f9650d8b-d5fc-4a7e-9e94-6d8239f9b4df
# ╠═d687707a-671d-44bb-a0ad-7f7b76018ae9
# ╠═7c341986-6527-4ded-8dae-53e79af1f19a
# ╠═b8213dcc-2d8b-491e-bb83-cf0a5fa38667
# ╠═d8af6824-043a-4ab9-b4d3-34440a43717a
# ╠═c6609c1c-b221-4e05-834f-bb2923ea5bd4
# ╠═b85937f6-c5f5-487f-9244-bea448c0d054
# ╠═3516264b-91e5-4751-9aa6-a38921d56362
# ╠═5d8c9ed4-9223-43ab-a1ef-c50e7a0cb22c
# ╠═3d863e14-851c-4670-a80c-c2898a870d44
# ╠═24cc824b-5244-4e22-92a9-284263c8ecbd
# ╠═86ad42aa-ef9c-4508-8233-3464547358e0
# ╠═2296e417-cbbc-4edd-92f6-ea34db573611
# ╠═6602335e-2922-4cdd-a6bc-ea3c7ad79e42
# ╠═3a15d0f7-6932-44ae-a37b-1d5cd8d4e9e5
# ╠═b73fe2f5-a95e-4e2b-94e3-d53c801f2b69
# ╠═574e4d7e-26b7-433a-9786-dd51d68a09f0
# ╠═777b7878-463d-491f-aa97-2aeb3ea4681a
# ╠═1906c863-cc25-48a2-b821-ee5e60789da5
# ╠═9ac1f6c9-fcbb-44fd-93c6-cd0dd0a85807
# ╠═60421479-e0b2-4add-86e9-6445a4af13ae
# ╠═f4ca0f94-e6cf-453e-b0a6-9e4c0798f6b6
# ╠═3ac3b3c4-e8a2-4a83-acbe-9fe711fe3fb4
# ╠═3e9dbeed-0229-458c-8a97-532648d662d6
# ╠═edc262eb-e82c-45ec-aac3-e8542b2d12d8
# ╠═47b61091-a23d-4d29-aa6a-f333d360d9be
# ╠═704dd474-a7a6-4dd8-a425-8d8008a8fb5e
# ╠═6dc07238-4c73-4a9b-88c4-a3094388ee82
# ╠═74ff4a0d-4c36-44f3-a238-b7403ad75333
# ╠═6c5f955b-b90d-4872-bcd7-91316bb0b9f9
# ╠═d4885d98-dece-4e56-99ab-2858f9c050eb
# ╠═440de24a-c966-467d-8f3c-111724ca7d9e
# ╠═0c85c2f3-e87b-42f3-bf0b-3d0f89aa9092
# ╠═b930ead7-02dd-42b0-ad45-29657f7e598b
# ╠═8503cc46-af66-4272-a3aa-b71615d85b1d
# ╠═6832e6ac-0c96-412f-947f-5e098083a0ac
# ╠═a6793577-00c9-425a-8006-fa1bb80062d1
# ╠═351878f6-c420-4e3d-8b4b-4d05e14484b3
# ╠═83da4182-f902-4704-8b68-0de73811d5b1
# ╠═05bf8b65-ff94-4d5c-9021-b75f50418d2c
# ╠═d9e0befd-c8c7-4c83-8fcb-3227c7a7a1b3
# ╠═29658d7c-35d1-41ec-9e03-04e1bd9986d6
# ╠═acdf7377-0e23-4b2b-8298-e767d5cc0844
# ╠═927117df-8cf3-4741-8c59-583648062546
# ╠═8dc2d5bf-8de1-4de5-8bb5-a657ecfb9178
# ╠═203bc150-c1e5-483a-941d-66e7bf7236a6
# ╠═31654e81-2f5c-410a-abf0-3dd1ede281bb
# ╠═477a3c4e-c253-45e8-8669-52b09bbcebd9
# ╠═95028509-0a9d-4b71-80e1-ec1faf163fc4
# ╠═be48a4f7-8f86-4491-8955-29875b655805
# ╠═80e0e5b5-c7f5-400b-bbf0-764722f4857d
# ╠═20778a44-9188-4319-95ce-8257c38d0e0f
# ╠═e89529b9-d8bf-4532-a3e6-bde93d7f51f7
# ╠═428f8ec3-f03b-420d-9b43-79a5126981a6
# ╠═5ed77470-0e60-42c3-aa72-e72e05ad7a6c
# ╠═66df1682-2814-45ff-b050-1cb6b00f1b5c
# ╠═1311bbee-937a-4471-b7e3-592e8a6a4221
# ╠═d73a8509-eabf-42b8-9922-d9f929fe4cef
# ╠═49982ee6-31eb-4e18-ace1-e6dafe360aaf
# ╠═33e6880e-1707-4197-a3af-1f55430b109e
# ╠═2f5f5e87-bf36-4a97-92bc-dbe87a92d9a1
# ╠═1c2d9bd7-6c45-4033-9438-d6f1a1577345
# ╠═dd001ba3-218f-421a-b0b3-9c38e5fe2c10
# ╠═05cae053-5774-40bc-b1be-55bf2be3a4bf
# ╠═b744c017-a08f-4fda-a1db-7dc8451f653e
# ╠═2d1fed46-5375-4d9c-a756-353294bb8c63
# ╠═9a36a705-9aec-45b4-aa46-dc57bd6c80fc
# ╠═1a89f0a7-e930-4c61-ab36-2aaf9d4f3b96
# ╠═bb06c2c6-bc46-444e-9148-00345a43f3a7
# ╠═dc791d87-5a59-42a8-bd55-df7f811e857e
# ╠═296a3b0c-eb5c-422c-98eb-5e537928b1aa
# ╠═506bd217-7c47-4922-b38c-496d98d317ef
# ╠═8c9ed2c0-7fab-47d8-9b82-890e177bf68b
# ╠═f6d05acc-8e0b-49d5-a9e0-a60928205b16
# ╠═e27773f3-ec05-4582-8d6d-0a1c62451377
# ╠═8f83876a-006f-4873-9706-58bc864c24db
# ╠═5e3fd4ab-120e-475a-8843-1e8af0f31ba0
# ╠═d2f8d36f-16fb-4149-b69b-c943b2251510
# ╠═6cc8e24a-dd5f-4a74-b32e-58fc47736486
# ╠═9bb8f392-e9d0-4702-88e7-ab4fc83d8d79
# ╠═ffad8e29-319f-4dbe-b621-fd515f7c37b6
# ╠═a00c834d-93d2-4cbf-8c9d-17d8440703e8
# ╠═d5c789bc-fc60-4467-b4cc-64fd62ca7a08
# ╠═8b840944-6768-479d-b99e-f134bf4f2ccd
# ╠═3fab9294-53aa-4775-8f6a-f2a7596b3227
# ╠═27f6123d-47ca-4cd4-8137-2b916d4e5a51
# ╠═327b9ea2-5740-4448-b6af-b983f6ff43a1
# ╠═c9d6377e-cb37-45d6-a00e-6ff2503934b9
# ╠═ceaf15d4-6f40-4ce3-be2e-e0d3dd175471
# ╠═b595d1b0-b9e5-463d-97af-e087a727404a
# ╠═de23c14a-484d-4c89-8bc8-9d33cf39c515
# ╠═73299f2c-1b8d-468a-aef2-5d58cbc264bb
# ╠═35c91e9e-8e27-48ff-abcd-0a7d7d6105bf
# ╠═fe1b38d5-f74b-43d6-9a86-f6d9aae664c6
# ╠═1e8608c4-1a5f-4ed1-b488-66b25ca7c922
# ╠═9796ecbc-e37e-42b9-823e-69e904f8a4c6
# ╠═dbaff6ba-4e78-4efb-ae77-b44344b981bf
# ╠═7043cca1-228d-412f-95d1-0af9cff6d514
# ╠═9f83adc3-d6aa-45c5-83c0-db51b39bd9d8
# ╠═6235d73a-3f6d-41c9-81ce-831a1b08754d
# ╠═6387a898-8150-4582-81bb-6c682375420c
# ╠═ec51f9ab-0983-476f-99ff-7d036fc23133
# ╠═db67d255-bd19-4827-9e65-216520e58fcd
# ╠═ea0835e0-dce7-4159-a5b5-b1489199d6c0
# ╠═feffb102-420e-4533-8333-26fcb60a112e
# ╠═b09828a5-adbc-4181-ae7f-f052f34e6bf5
# ╠═a046cd4b-f985-4fbe-9549-cc9e7b1ea887
# ╠═ec6459f4-9a2a-4205-8285-dade22d3abbc
# ╠═b812ce2a-14ab-4238-b4ff-ba6c85834336
# ╠═ac5a548a-b0b2-42ba-8f67-709b99f72cf4
# ╠═488112c3-c3ed-4ac0-b1d6-1d53be88131f
# ╠═f5c21e7d-ef41-4e5b-b512-3bf9373144a3
# ╠═6a3acf57-45b2-4841-8325-83e955e6dc72
# ╠═3beb5d0f-85f4-4ff6-96a4-29f0b1a9f643
# ╠═102ff106-33e3-41fe-9df3-209c958be15f
# ╠═a1d1d9e3-d86f-4498-94c4-c6769c09ad7d
# ╠═363bb90b-314b-4fbd-883f-c25c3d968700
# ╠═29c9c6f9-cf0c-470e-8791-18fb46de051b
# ╠═d91889c9-eaf1-4f8c-be1b-afa5b60c1a50
# ╠═e287127f-3042-4b2b-88bf-0dfa27ed34db
# ╠═fcefa967-876b-426a-83e6-ecfcaad5c6ef
# ╠═fb71ae63-0391-4605-b561-4862346a8eb4
# ╠═70dff569-31f4-4b20-bf9d-8b7b2fe8166a
# ╠═0b8ed630-978b-4dc1-abab-2ac439aa6f76
# ╠═785388d2-a417-444b-a1ce-02fa2438d585
# ╠═bfcaf949-79ac-415a-8725-61486c5e8d68
# ╠═d454d0b8-b700-477c-a700-5d854b85e94c
# ╠═7cc9b806-0865-40b7-8873-8791f6610c04
# ╠═64842ec8-2dbd-4064-afce-330690c064b2
# ╠═311cd8e6-bce3-4b74-81b8-cba4535dde0f
# ╠═8fe23f8a-8d4c-4a35-8603-9682e533ae37
# ╠═3ccf8ceb-b42a-4db0-81a8-f9f1157c39ee
# ╠═694824e8-d798-4455-a5bb-7593d4aac5e4
# ╠═3ce8e7b1-9183-4648-91d4-cab44588e14c
# ╠═9306fa27-1679-4896-b03f-7adb78af67ba
# ╠═c13e099f-5425-4ffc-b452-9c0403251a3f
# ╠═d4c4aed1-b7ca-4ff3-853b-792324235f52
# ╠═a70f0481-acdd-4427-b39f-8b1c8301083a
# ╠═ecdccbf2-a102-40ce-8ed2-a4b8ddf29332
# ╠═8f9598c2-13f0-41e1-8150-efa27e78454f
# ╠═fe4eb90e-30af-404b-9fbf-1be150bba8f4
# ╠═038dbc83-4268-4a3e-a141-bec2ab4f459c
# ╠═ceb65c0b-133d-4acc-9039-7b21aa1c6d95
# ╠═fb93e546-fdbd-46db-b20f-a4a9474ec55e
# ╠═4f9ce79b-e18b-428f-acf6-86e9d1c024bd
# ╠═3159e972-bb5d-4ecd-a8e9-25ccf27b87a8
# ╠═6eafb819-a69c-4fd8-9f79-a7871cc4f237
# ╠═5a4b07bb-98e8-4690-a76f-e8233790ae23
# ╠═7f21d05e-25b8-4987-8c25-24e233ca8fb0
# ╠═a9ac8cfc-7a06-4d12-a00f-74bc2056e860
# ╠═f4da16ac-9e94-455a-87af-a4c4a87b864a
# ╠═3a5dd253-75a8-4502-8d40-c0ff4f72e4af
# ╠═ee4b1560-1f51-420c-87cb-66d94a1689e6
# ╠═e15c142d-6bd8-4c06-ae6b-403f73645632
# ╠═f4e5432b-ff24-4690-b849-aca10c3ed23e
# ╠═3e191b6f-8d42-4c37-bec9-593412429469
# ╠═90aa2da3-c11c-412c-bb84-1fb7648f97ec
# ╠═e4cee5d5-5389-4877-99bb-d481fe4ff372
# ╠═585ffa67-9615-4d8a-a9ff-1516fa2b66e0
# ╠═9cc61f01-2469-41fa-b4bf-11c0b8bee278
# ╠═89ba50a8-6475-4142-9e30-a60a8ed3e7c8
# ╠═a81728bd-184b-41f0-bad7-fe04b18f45bc
# ╠═fd5b95e0-4eb7-45fa-94a6-582bea6834a1
# ╠═a862955d-b00e-4c84-90a3-3795eed0fe38
# ╠═56b335e5-4ab9-4bef-af59-bbb99f28e098
# ╠═a06c8f73-23af-476d-8900-915ff937989b
# ╠═377b7210-a98e-4bf2-aef6-5462e5d513ca
# ╠═a20d29b0-6674-4efe-b500-2dd82979e2b8
# ╠═03b6fb0f-d726-4185-8cbf-1fd2ae44af10
# ╠═40698c47-e271-4729-9d07-aa79662fdd6c
# ╠═c5df8c92-1bff-4e7a-93c9-d3c0d5b9e221
# ╠═02a93a00-ec59-4bc0-9489-88fdf97475d7
